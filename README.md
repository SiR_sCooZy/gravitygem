# GravityGem

## About
This game is a classic platformer with a little twist:

![Gravity in regular direction](./resources/ingame/gameplay1.png)

Gravity can be inverted such that the player "falls up" instead of down while being in the air.
This allows to land and run on the bottom of the platforms.

![Gravity in inverted direction](./resources/ingame/gameplay2.png)

Avoid landing on the green spots to not get stuck on them with your boots.

![Gravity in inverted direction](./resources/ingame/gameover.png)

Also avoid falling and running out of the screen. This will also make you lose ;). \
That's it! Have fun playing. 

You can either build the game yourself (see below) or download the ready-to-play Windows executable from [here](https://drive.google.com/file/d/10f5ZavobmmlQ2sw6oHP_Nm1rrDVlgo1z/view?usp=sharing). Beware, there may be virus warnings :/.

## How to Play

### Main Controls
**A - walk left** \
**D - walk right** \
**SPACE - jump (when on platform) / invert gravity (when in the air)** 

Double jumps are not allowed as well as inverting gravity multiple times without landing in between.

### Other Controls
**F1 - toggle fullscreen** \
**ESCAPE - quit** \
**R - restart game**


## Implementation

The game is written in pure Python 3 and should work for versions >= 3.8. \
The only dependency is Pillow to load up the player sprite. \
The graphics are implemented in Tkinter.

### Compilation

If you want to compile the game yourself (on Windows), 
you can use Nuitka with the command stored in [nuitka_build_command.txt](./nuitka_build_command.txt).


