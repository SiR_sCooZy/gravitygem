from dataclasses import dataclass


@dataclass
class PlayerControls:
    jump: str
    left: str
    right: str
