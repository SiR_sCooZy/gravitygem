from __future__ import annotations

from dataclasses import dataclass

from src.player_controls import PlayerControls


@dataclass
class Settings:

    FULLSCREEN: bool = True
    WIDTH: int | None = None    # 1000
    HEIGHT: int | None = None    # 500
    WORLD_SPEED_X: float = 0.2
    WORLD_ACCELERATION_X: float = 0.005

    PLAYER_GRAVITY: float = 4.0
    PLAYER_CONTROLS = PlayerControls(jump="space", left="a", right="d")
    PLAYER_JUMP_SPEED: float = 1.5
    PLAYER_SPEED_X_MAX: float = 0.2
    PLAYER_WIDTH: float = 0.05
    PLAYER_HEIGHT: float = 0.1
    PLAYER_UP_SPRITE: str = "player_up.png"
    PLAYER_DOWN_SPRITE: str = "player_down.png"

    PARTICLES_PER_SECOND: int = 40
    PARTICLE_WIDTH: float = 0.007
    PARTICLE_HEIGHT: float = 0.014
    PARTICLE_GRAVITY: float = 1.0
    PARTICLE_SPEED_X_MAX: float = 0.01

    BACKGROUND_COLOR: str = "#fffb82"
    BACKGROUND_COLOR_GAME_OVER: str = "#ff4a4a"
    WALL_COLOR: str = "#52063c"
    SPIKE_COLOR: str = "#166701"
    TEXT_COLOR: str = "#c9c9c9"
    PARTICLE_COLOR: str = "#ffffff"

    WALL_WIDTH_MIN: float = 0.05
    WALL_WIDTH_MAX: float = 0.25
    WALL_HEIGHT: float = 0.05
    WALL_X_OFFSET_MIN: float = 0.08
    WALL_X_OFFSET_MAX: float = 0.20
    WALL_Y_OFFSET_MIN: float = 0.00
    WALL_Y_OFFSET_MAX: float = 0.22

    P_SPIKES: float = 0.5


settings = Settings()
