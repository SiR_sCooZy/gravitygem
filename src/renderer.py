from __future__ import annotations

import tkinter as tk
from pathlib import Path
from tkinter import ALL

from typing import TYPE_CHECKING

from src.player_controls import PlayerControls
from src.settings import settings

if TYPE_CHECKING:
    from src.game_object.game_object import GameObject
    from src.game_object.player import Player
    from src.world import World

from PIL import Image, ImageTk


class Renderer:

    def __init__(self, world: "World"):
        self._world = world
        self._window = tk.Tk()
        self._window.title("GravityGames")
        self._fullscreen = settings.FULLSCREEN
        self._set_fullscreen(self._fullscreen)
        self._window.config(cursor="none")
        self._window.bind("<KeyPress-F1>", lambda _: self._toggle_fullscreen())
        self._window.bind("<Escape>", lambda _: self._quit_request())
        self._window.bind("<r>", lambda _: self.restart_game())
        w, h = self._window.winfo_screenwidth(), self._window.winfo_screenheight()
        self._w = w if settings.WIDTH is None else settings.WIDTH
        self._h = h if settings.HEIGHT is None else settings.HEIGHT
        self._canvas = tk.Canvas(self._window, bg=settings.BACKGROUND_COLOR,
                                 height=self._h, width=self._w)
        self._quit: bool = False
        self._canvas.pack()
        self._sprites_cache = dict()
        self._resource_dir = (Path(__file__).parent.parent / "resources" / "sprites").resolve()

    @property
    def quit(self) -> bool:
        return self._quit

    def render_sprite(self, sprite_name: str, x: float, y: float, width: float, height: float) -> None:
        x_px, y_px = self._convert_coordinate(x, y)
        w, h = self._convert_coordinate(width, height)
        if (sprite_image := self._sprites_cache.get((sprite_name, w, h), None)) is None:
            sprite_image = Image.open(self._resource_dir / sprite_name).resize((w, h))
            sprite_image = ImageTk.PhotoImage(sprite_image)
            self._sprites_cache[(sprite_name, w, h)] = sprite_image
        self._canvas.create_image(x_px, y_px, image=sprite_image, anchor="nw", state="normal")

    def render_rect(self, x: float, y: float, width: float, height: float, color: str) -> None:
        x_px, y_px = self._convert_coordinate(x, y)
        x2_px, y2_px = self._convert_coordinate(x + width, y + height)
        self._canvas.create_rectangle(x_px, y_px, x2_px, y2_px, fill=color, outline="")

    def render_circle(self, x: float, y: float, width: float, height: float, color: str) -> None:
        x_px, y_px = self._convert_coordinate(x, y)
        x2_px, y2_px = self._convert_coordinate(x + width, y + height)
        self._canvas.create_oval(x_px, y_px, x2_px, y2_px, fill=color, outline="")

    def render_text(self, x: int, y: int, size: int, text: str, font: str = "Arial"):
        self._canvas.create_text(x, y, fill=settings.TEXT_COLOR, font=f"{font} {size}", text=text)

    def render(self, game_objects: list["GameObject"], game_over: bool, score: int) -> None:
        color = settings.BACKGROUND_COLOR_GAME_OVER \
            if game_over or not self._world.game_started else settings.BACKGROUND_COLOR
        self._canvas.configure(bg=color)
        self._canvas.delete(ALL)
        game_objects.sort(key=lambda go: go.z)
        for game_object in game_objects:
            game_object.render(self)
        score_str = str(score)
        self.render_text(15 + len(score_str) * 10, 30, size=30, text=score_str)
        if game_over:
            self.render_text(self._w / 2, self._h / 2 - 50, size=60, text="Game Over")
        if game_over or not self._world.game_started:
            if game_over:
                text, y = "Press 'R' to Restart", self._h / 2 + 50
            else:
                text, y = "Press 'R' to Start", self._h / 2
            self.render_text(self._w / 2, y, size=60, text=text)
        self._window.update_idletasks()
        self._window.update()

    def setup_player_controls(self, player: "Player", controls: PlayerControls) -> None:
        self._window.bind(f'<KeyPress-{controls.jump}>', lambda _: player.jump())
        self._window.bind(f'<KeyPress-{controls.right}>', lambda _: player.walk('right'))
        self._window.bind(f'<KeyRelease-{controls.right}>', lambda _: player.walk('right_clear'))
        self._window.bind(f'<KeyPress-{controls.left}>', lambda _: player.walk('left'))
        self._window.bind(f'<KeyRelease-{controls.left}>', lambda _: player.walk('left_clear'))

    def restart_game(self):
        if self._world.game_started:
            self._world.restart()
        else:
            self._world.game_started = True

    def _convert_coordinate(self, x: float, y: float) -> tuple[int, int]:
        return round(x * self._w), round(y * self._h)

    def _set_fullscreen(self, fullscreen: bool) -> None:
        self._window.attributes("-fullscreen", fullscreen)

    def _toggle_fullscreen(self) -> None:
        self._fullscreen = not self._fullscreen
        self._set_fullscreen(self._fullscreen)

    def _quit_request(self):
        self._quit = True
