from __future__ import annotations

import math
import random
import time

from src.game_object.game_object import GameObject
from src.game_object.particle import Particle
from src.game_object.player import Player
from src.renderer import Renderer
from src.settings import settings
from src.game_object.spike import Spike, SpikeDirection
from src.game_object.wall import Wall


class World:

    def __init__(self):
        self._renderer = Renderer(self)
        self._cur_platform_x: float = -1
        self._cur_platform_y: float = -1
        self._cur_offset_y: float = -1
        self._cur_spike_direction: SpikeDirection = SpikeDirection.NONE
        self._delta_time = 0
        self._collision_objects: list[GameObject] = []
        self._player: Player | None = None
        self._particles: list[Particle] = []
        self._new_particles_remains: float = 0
        self._v_x = settings.WORLD_SPEED_X
        self.game_started: bool = False
        self.gravity_direction: float = 1
        self.restart()

    @property
    def v_x(self) -> float:
        return self._v_x

    def restart(self) -> None:
        self._v_x = settings.WORLD_SPEED_X
        self._new_particles_remains = 0
        self.gravity_direction = 1
        self._cur_platform_x = 0.05
        self._cur_platform_y = 0.5 - settings.WALL_HEIGHT / 2
        self._cur_offset_y = 0
        self._cur_spike_direction = SpikeDirection.NONE
        self._collision_objects = self._spawn_platforms(start_width=0.3)
        self._player = self._spawn_player()
        self._particles = self._spawn_particles(settings.PARTICLES_PER_SECOND, y_range=(-0.1, 1.1))

    def run(self) -> None:
        while not self._renderer.quit:
            start_frame = time.perf_counter()
            self._update_game_objects()
            render_objects = self._collision_objects + self._particles + [self._player]
            self._renderer.render(render_objects, not self._player.alive, self._player.score)
            end_frame = time.perf_counter()
            self._delta_time = end_frame - start_frame

    def _update_game_objects(self) -> None:
        if self._player.alive and self.game_started:
            self._v_x += settings.WORLD_ACCELERATION_X * self._delta_time
            new_particles = settings.PARTICLES_PER_SECOND * self._delta_time + self._new_particles_remains
            n_new_particles = math.floor(new_particles)
            self._new_particles_remains = new_particles - n_new_particles
            if n_new_particles > 0:
                self._particles += self._spawn_particles(n_new_particles)
            self._cur_platform_x -= self._delta_time * self._v_x
            self._player.update(self._collision_objects, self._delta_time)
            [go.update(self._collision_objects, self._delta_time) for go in self._collision_objects]
            [p.update(self._collision_objects, self._delta_time) for p in self._particles]
            self._collision_objects = [go for go in self._collision_objects if go.alive]
            self._particles = [p for p in self._particles if p.alive]
            if (new_walls := self._spawn_platforms()) is not None:
                self._collision_objects += new_walls

    def _spawn_particles(self, amount: int, y_range: tuple[float, float] | None = None) -> list[Particle]:
        particles = []
        if y_range is None:
            if self.gravity_direction == 1:
                y_range = (-0.19 - settings.PARTICLE_HEIGHT, -settings.PARTICLE_HEIGHT)
            else:
                y_range = (1.0, 1.19)
        for _ in range(amount):
            x = self.random_float(0, 1)
            y = self.random_float(*y_range)
            particles.append(Particle(x=x, y=y, w=settings.PARTICLE_WIDTH, h=settings.PARTICLE_HEIGHT, world=self))
        return particles

    def _spawn_platforms(self, spawn_x_threshold: float = 1.1,
                         start_width: float | None = None) -> list[GameObject] | None:
        walls = None
        if self._cur_platform_x < spawn_x_threshold:
            walls = []
            while self._cur_platform_x < spawn_x_threshold:
                width = self.random_float(settings.WALL_WIDTH_MIN, settings.WALL_WIDTH_MAX)
                if start_width is not None:
                    width = start_width
                walls.append(Wall(x=self._cur_platform_x,
                                  y=self._cur_platform_y,
                                  w=width,
                                  h=settings.WALL_HEIGHT,
                                  world=self))
                if start_width is None:
                    if (spike := self._spawn_spike(width)) is not None:
                        walls.append(spike)
                start_width = None
                self._cur_platform_x += width
                offset_x = self.random_float(settings.WALL_X_OFFSET_MIN, settings.WALL_X_OFFSET_MAX)
                self._cur_platform_x += offset_x
                if self.coin_toss():
                    # place below
                    direction = 1
                    y_offset_max = min(0.85 - settings.WALL_HEIGHT - self._cur_platform_y, settings.WALL_Y_OFFSET_MAX)
                else:
                    # place above
                    direction = -1
                    y_offset_max = min(self._cur_platform_y - 0.15, settings.WALL_Y_OFFSET_MAX)
                self._cur_offset_y = direction * self.random_float(settings.WALL_Y_OFFSET_MIN, y_offset_max)
                self._cur_platform_y += self._cur_offset_y
        return walls

    def _spawn_spike(self, width: float) -> Spike | None:
        if self.with_probability(settings.P_SPIKES):
            spike_h = settings.WALL_HEIGHT / 3
            # if wall is on top of world, make it unlikely that spike spawns on the bottom of it
            # and vice versa (linearly inversely proportional)
            p_above = 1 - self._cur_platform_y
            spike_y = None
            max_spike_offset = settings.WALL_Y_OFFSET_MAX / 3
            if self.with_probability(p_above):
                # if platform is far below previous one spikes can't be on top (unplayable)
                if self._cur_spike_direction != SpikeDirection.UP and self._cur_offset_y < max_spike_offset:
                    spike_y = self._cur_platform_y - 0.01
                    self._cur_spike_direction = SpikeDirection.UP
            else:
                # if platform is far above previous one spikes can't be on bottom (unplayable)
                if self._cur_spike_direction != SpikeDirection.DOWN and self._cur_offset_y < -max_spike_offset:
                    spike_y = self._cur_platform_y + settings.WALL_HEIGHT - spike_h + 0.01
                    self._cur_spike_direction = SpikeDirection.DOWN
            if spike_y is not None:
                return Spike(x=self._cur_platform_x, y=spike_y, w=width, h=spike_h, world=self)
        self._cur_spike_direction = SpikeDirection.NONE
        return None

    def _spawn_player(self) -> Player:
        y = 0.5 - settings.WALL_HEIGHT / 2 - settings.PLAYER_HEIGHT
        player = Player(x=0.15, y=y, w=settings.PLAYER_WIDTH, h=settings.PLAYER_HEIGHT, world=self)
        self._renderer.setup_player_controls(player, settings.PLAYER_CONTROLS)
        return player

    @staticmethod
    def random_float(min_val: float, max_val: float):
        return random.uniform(min_val, max_val)

    @staticmethod
    def with_probability(p: float) -> bool:
        return World.random_float(0, 1) < p

    @staticmethod
    def coin_toss() -> bool:
        return World.random_float(0, 1) < 0.5
