from __future__ import annotations

import math
from dataclasses import dataclass, field

from src.game_object.game_object import GameObject
from src.game_object.spike import Spike
from src.settings import settings


@dataclass
class Player(GameObject):
    sprite: str = settings.PLAYER_UP_SPRITE
    jump_speed: float = settings.PLAYER_JUMP_SPEED

    _v_x: float = field(default=0, init=False)
    _v_y: float = field(default=0, init=False)
    _is_jumping: bool = field(default=False, init=False)
    _can_invert: bool = field(default=False, init=False)
    _score: int = field(default=0, init=False)
    _sprite_up: str = settings.PLAYER_UP_SPRITE
    _sprite_down: str = settings.PLAYER_DOWN_SPRITE

    def __post_init__(self):
        self.z = 100

    @property
    def score(self) -> int:
        return math.ceil(self._score / 10)

    def update(self, game_objects: list[GameObject], delta_time: float) -> list[GameObject] | None:
        self._score += round(delta_time * 1000)
        at = settings.PLAYER_GRAVITY * self.world.gravity_direction * delta_time
        self._v_y += at
        self.y += self._v_y * delta_time
        self.x += self._v_x * delta_time
        for go in game_objects:
            if self.handle_collision(go, x_axis=False):
                self._is_jumping = False
                self._can_invert = True
                self._v_y = 0
                if isinstance(go, Spike):
                    self.alive = False
            self.handle_collision(go, x_axis=True)
        if self.y + self.h < -0.05 or self.y > 1.05 or self.x + self.w < -0.05 or self.x > 1.05:
            self.alive = False
        return None

    def jump(self) -> None:
        if not self._is_jumping and self._v_y == 0:
            self._v_y = settings.PLAYER_JUMP_SPEED * -self.world.gravity_direction
            self._is_jumping = True
        elif self._can_invert and self._v_y != 0:
            self.world.gravity_direction *= -1
            self.sprite = self._sprite_up if self.world.gravity_direction == 1 else self._sprite_down
            self._can_invert = False

    def walk(self, direction: str) -> None:
        if direction == "right":
            self._v_x = settings.PLAYER_SPEED_X_MAX
        if direction == "left":
            self._v_x = -settings.PLAYER_SPEED_X_MAX
        if direction == "right_clear" and self._v_x > 0:
            self._v_x = 0
        if direction == "left_clear" and self._v_x < 0:
            self._v_x = 0
