from __future__ import annotations


from dataclasses import dataclass

from src.game_object.game_object import GameObject
from src.settings import settings


@dataclass
class Wall(GameObject):
    color: str = settings.WALL_COLOR
    z: int = 10

    def update(self, game_objects: list[GameObject], delta_time: float) -> list[GameObject] | None:
        self.x = self.x - delta_time * self.world.v_x
        if self.x + self.w < -0.1:
            self.alive = False
        return None
