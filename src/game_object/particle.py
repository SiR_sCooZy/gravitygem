from __future__ import annotations

from dataclasses import dataclass, field

from src.game_object.game_object import GameObject
from src.renderer import Renderer
from src.settings import settings

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from src.world import World


@dataclass
class Particle(GameObject):
    color: str = settings.PARTICLE_COLOR
    z: int = 5
    world: "World" = None

    _v_x: float = field(default=0, init=False)
    _v_y: float = field(default=0, init=False)

    def __post_init__(self):
        self._v_x = self.world.random_float(-settings.PARTICLE_SPEED_X_MAX,
                                            settings.PARTICLE_SPEED_X_MAX)

    def update(self, game_objects: list[GameObject], delta_time: float) -> list[GameObject] | None:
        self.x += self._v_x * delta_time
        at = settings.PARTICLE_GRAVITY * self.world.gravity_direction * delta_time
        self._v_y += at
        self.y += self._v_y * delta_time
        if self.x + self.w < -0.2 or self.y + self.w < -0.2 or self.y > 1.2:
            self.alive = False
        return None

    def render(self, renderer: Renderer) -> None:
        renderer.render_circle(self.x, self.y, self.w, self.h, self.color)
