from __future__ import annotations

from dataclasses import dataclass
from enum import Enum

from src.game_object.wall import Wall
from src.settings import settings


class SpikeDirection(Enum):
    NONE = 0
    UP = 1
    DOWN = 2


@dataclass
class Spike(Wall):
    color: str = settings.SPIKE_COLOR
    z: int = 20
