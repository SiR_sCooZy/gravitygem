from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass

from src.renderer import Renderer

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from src.world import World


@dataclass
class GameObject(ABC):
    x: float
    y: float
    w: float
    h: float
    world: "World"
    color: str | None = None
    sprite: str | None = None
    # render order (higher = more to the front)
    z: int = 0
    alive: bool = True

    @abstractmethod
    def update(self, game_objects: list[GameObject], delta_time: float) -> list[GameObject] | None:
        ...

    def render(self, renderer: Renderer) -> None:
        if self.sprite is None:
            renderer.render_rect(self.x, self.y, self.w, self.h, self.color)
        else:
            renderer.render_sprite(self.sprite, self.x, self.y, self.w, self.h)

    def can_collide(self, other: GameObject) -> bool:
        # default is True if not specified
        can_collide_dict = {}
        self_class, other_class = sorted([self.__class__.__name__, other.__class__.__name__])
        can_collide = can_collide_dict.get((self_class, other_class), True)
        return can_collide

    def collides(self, other: GameObject, margin: float = 0) -> bool:
        if self == other:
            return False
        if self.can_collide(other):
            if self.x + self.w + margin <= other.x or self.x >= other.x + other.w + margin:
                return False
            if self.y + self.h + margin <= other.y or self.y >= other.y + other.h + margin:
                return False
            return True
        else:
            return False

    def handle_collision(self, other: GameObject, x_axis: bool, margin: float = 0) -> bool:
        if self.collides(other, margin):
            # default is True if not specified
            handle_collision_dict = {}
            self_class, other_class = sorted([self.__class__.__name__, other.__class__.__name__])
            if handle_collision_dict.get((self_class, other_class), True):
                if x_axis:
                    if (self.x + self.w > other.x - margin) and (self.x + self.w < other.x + other.w + margin):
                        self.x = other.x - self.w - margin
                    if (self.x < other.x + other.w + margin) and (self.x > other.x - margin):
                        self.x = other.x + other.w + margin
                else:
                    if (self.y + self.h > other.y - margin) and (self.y + self.h < other.y + other.h + margin):
                        self.y = other.y - self.h - margin
                    if (self.y < other.y + other.h + margin) and (self.y > other.y - margin):
                        self.y = other.y + other.h + margin
            return True
        return False
